﻿Public Class Form1
    Dim retorno, tipo As Integer
    Dim modelo, conexao, baudRate As String
    ' Botão de Status
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

        retorno = FunctionsDll.StatusImpressora(4)
        Select Case retorno
            Case 0
                TextBox3.Text = "Status de Impressora: Funcionalidade Total." + System.Environment.NewLine + "Status de Gaveta: Gaveta Fechada." + System.Environment.NewLine + "Status de Papel: Papel Suficiente."
            Case 1
                TextBox3.Text = "Status de Impressora: Funcionalidade Total." + System.Environment.NewLine + "Status de Gaveta: Gaveta Aberta." + System.Environment.NewLine + "Status de Papel: Papel Suficiente."
            Case 2
                TextBox3.Text = "Status de Impressora: Tampa da Impressora Aberta." + System.Environment.NewLine + "Status de Gaveta: Gaveta Fechada." + System.Environment.NewLine + "Status de Papel: Papel Suficiente."
            Case 3
                TextBox3.Text = "Status de Impressora: Tampa da Impressora Aberta." + System.Environment.NewLine + "Status de Gaveta: Gaveta Aberta." + System.Environment.NewLine + "Status de Papel: Papel Suficiente."
            Case 4
                TextBox3.Text = "Status de Impressora: Funcionalidade Total." + System.Environment.NewLine + "Status de Gaveta: Gaveta Fechada." + System.Environment.NewLine + "Status de Papel: Pouco Papel."
            Case 5
                TextBox3.Text = "Status de Impressora: Funcionalidade Total." + System.Environment.NewLine + "Status de Gaveta: Gaveta Aberta." + System.Environment.NewLine + "Status de Papel: Pouco Papel."
            Case 12
                TextBox3.Text = "Status de Impressora: Funcionalidade Total." + System.Environment.NewLine + "Status de Gaveta: Gaveta Fechada." + System.Environment.NewLine + "Status de Papel: Fim de Papel."
            Case 13
                TextBox3.Text = "Status de Impressora: Funcionalidade Total." + System.Environment.NewLine + "Status de Gaveta: Gaveta Aberta." + System.Environment.NewLine + "Status de Papel: Fim de Papel."
            Case Else
                TextBox3.Text = "Resultado: " + retorno + System.Environment.NewLine + System.Environment.NewLine + "Erro não Registrado. Valor verificar."
        End Select

    End Sub
    ' Abertura de Gaveta
    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        retorno = FunctionsDll.InicializaImpressora()
        retorno = FunctionsDll.AbreGaveta()
        If retorno = 0 Then
            TextBox3.Text = "Gaveta Aberta com Sucesso."
        Else
            TextBox3.Text = "Algo deu errado. Por favor, verificar."
        End If
    End Sub
    ' ´Código de Barras
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        retorno = FunctionsDll.InicializaImpressora()
        retorno = FunctionsDll.ImpressaoTexto("Todos os Codigos.", 1, 8, 0)
        retorno = FunctionsDll.AvancaPapel(4)
        retorno = FunctionsDll.ImpressaoTexto("UPC-A", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoCodigoBarras(0, "01234567890", 90, 2, 4)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("UPC-E", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoCodigoBarras(1, "01234567890", 90, 2, 4)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("JAN 13 / EAN 13", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoCodigoBarras(2, "001234567890", 90, 2, 4)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("JAN 8 / EAN 8", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoCodigoBarras(3, "01234567", 90, 2, 4)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("CODEBAR", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoCodigoBarras(6, "A1234+5678B", 90, 2, 4)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("CODE 39", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoCodigoBarras(4, "*0123456789*", 90, 2, 4)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("CODE 93", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoCodigoBarras(4, "*TESTECODE93*", 90, 2, 4)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("ITF", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoCodigoBarras(5, "1234567890", 90, 2, 4)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("CODE 128", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoCodigoBarras(8, "{C35150661099008000141593515066109900800014159", 90, 2, 4)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("QR CODE", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        Dim qrcodee As String = "http://homnfce.sefaz.am.gov.br/nfceweb/consutarNFCe.jsp?chNFe=13161104240370000742652000000445901000445906&nVersao=100&tpAmb=2&dhEmi=323031362D31312D31375430393A33373A31372D30323A3030&vNF=11.98&vICMS=0.00&digVal=497A762B634A4439374258533568546751753541375330554E436B3D&cIdToken=000001&cHashQRCode=2A1303EDAEFAB7345F841BDC7A6BD75B1DD0866E"
        retorno = FunctionsDll.ImpressaoQRCode(qrcodee, 5, 2)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.Corte(1)

        If retorno = 0 Then
            TextBox3.Text = "Impressão concluida com Sucesso."
        Else
            TextBox3.Text = "Algo deu errado. Favor verificar."
        End If
    End Sub
    ' Cupom Padrão
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        retorno = FunctionsDll.InicializaImpressora()
        retorno = FunctionsDll.ImpressaoTexto("Elgin Manaus", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("Elgin S/A", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("Rua Abiurana, 579 Distrito Industrial Manaus - AM", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("CNPJ 14.200.166/0001-66 IE 111.111.111.111 IM", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("----------------------------------------------------------------", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("Extrato No 000000", 1, 8, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("CUPOM FISCAL ELETRONICO - SAT", 1, 8, 0)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("= T E S T E =", 1, 8, 0)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("----------------------------------------------------------------", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("CPF/CNPJ do Consumidor:", 0, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("----------------------------------------------------------------", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("# | COD | DESC | QTD | UN | VL UN R$ | (VL TR R$)* | VL ITEM R$", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("----------------------------------------------------------------", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("001 000000000000 Impressora i9 0 cx X 0,00 (0,00)           0,00", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("rateio de desconto sobre subtotal:                         -0,00", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("001 000000000000 Impressora i7 0 cx X 0,00 (0,00)           0,00", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("rateio de desconto sobre subtotal:                         -0,00", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("Total bruto de itens                                        0,00", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("Desconto sobre subtotal                                    -0,00", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("TOTAL R$                                   00,00", 0, 8, 0)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("Cartao de Debito                                           00,00", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("*ICMS a ser recolhido conforme LC 000/0000  -  Simples Nacional*", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("----------------------------------------------------------------", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("OBSERVACOES DO CONTRIBUINTE", 0, 1, 0)
        retorno = FunctionsDll.AvancaPapel(2)
        retorno = FunctionsDll.ImpressaoTexto("*Valor aproximado dos tributos do item", 0, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("Valor aproximado dos tributos deste cupom R$                0,00", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("(conforme Lei Fed 12.741/2012)", 0, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("----------------------------------------------------------------", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("SAT No. 000.000.000", 1, 8, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("00/00/0000 - 00:00:00", 1, 8, 0)
        retorno = FunctionsDll.AvancaPapel(2)
        Dim code128 As String = "{C35150661099008000141593515066109900800014159"
        retorno = FunctionsDll.ImpressaoCodigoBarras(8, code128, 80, 2, 1)
        retorno = FunctionsDll.AvancaPapel(2)
        Dim qrcode As String = "35141146377222003730599000004630000853254753|20141105134843|637.00||H0iMysWj\r\n" +
                "M9zOXjaxkpPjqk7Q0Fp4RFvWkC0jLngU8o/pg5WpjhiVU2i/7BnIDz/WU3bMd9Cg6qWHvyn11e+\r\n" +
                "qE7VpUjfZFPiczrrxPOqke3kdaX3y2db0/xebRcLUn1+PO2fvKK/7tAPMUiXvb1YDH6DqHVBhoM5A\r\n" +
                "7u3P5RZrHvl1dfaJumLbe5uejKmq4rDNdOI5EVReJ5q1O6asOzwd8O0JzUGIf9aSdcClKWY3Xqt+0I\r\n" +
                "WGjX61Kb8bEobAe4wV69hPpCApoAwDzrjuB110aUa2Q2aR/X3GWgqs8GTNvwQVDYNexuo3Pk\r\n" +
                "67uo/hUr8tLMb2K15SIqoCBfya6sUIvA=="
        retorno = FunctionsDll.ImpressaoQRCode(qrcode, 5, 2)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("Consulte o QR Code pelo aplicativo " & Chr(34) & "De olho na nota" & Chr(34), 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(1)
        retorno = FunctionsDll.ImpressaoTexto("disponivel na AppStore (Apple) e PlayStore (Android)", 1, 1, 0)
        retorno = FunctionsDll.AvancaPapel(3)
        retorno = FunctionsDll.Corte(1)

        If retorno = 0 Then
            TextBox3.Text = "Impressão concluda com sucesso."
        Else
            TextBox3.Text = "Algo deu errado. Favor verificar."
        End If

    End Sub
    ' Conexão
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ' Tratando Modelo.
        If ComboBox1.Text = "• Impressora i9" Then
            modelo = "i9"
        ElseIf ComboBox1.Text = "• Impressora i7" Then
            modelo = "i7"
        ElseIf ComboBox1.Text = "• Impressora i10" Then
            modelo = "i10"
        ElseIf ComboBox1.Text = "• Impressora Fitpos" Then
            modelo = "Fitpos"
        ElseIf ComboBox1.Text = "• Impressora BK-T681" Then
            modelo = "BK-T681"
        ElseIf ComboBox1.Text = "• Impressora K" Then
            modelo = "K"
        Else
            TextBox3.Text = "Modelo Inválido. Favor Verificar."
        End If
        ' Tratando Conexão.
        If ComboBox2.Text = "• Comunicação Serial" Then
            tipo = 2
            conexao = TextBox1.Text
            baudRate = TextBox2.Text
            If ComboBox2.Text = "• Comunicação Serial" And TextBox1.Text = "" And TextBox1.Text = "" Then
                MessageBox.Show("Favor Informar a Porta Serial e o BaudRate.")
            End If
        Else
                tipo = 1
            conexao = "USB"
            baudRate = 0
        End If

        retorno = FunctionsDll.AbreConexaoImpressora(tipo, modelo, conexao, baudRate)

        Select Case retorno
            Case 0
                Label5.Text = "STATUS DE CONEXÃO: PORTA ABERTA COM SUCESSO."
            Case -9999
                Label5.Text = "STATUS DE CONEXÃO: ERRO DESCONHECIDO."
            Case -2
                Label5.Text = "STATUS DE CONEXÃO: TIPO DE CONEXÃO INVÁLIDA."
            Case -3
                Label5.Text = "STATUS DE CONEXÃO: MODELO INVÁLIDO."
            Case -4
                Label5.Text = "STATUS DE CONEXÃO: PORTA DE CONEXÃO FECHADA."
            Case -5
                Label5.Text = "STATUS DE CONEXÃO: CONEXÃO NEGADA."
            Case -11
                Label5.Text = "STATUS DE CONEXÃO: BAUDRATE INVÁLIDO."
            Case 1
                Label5.Text = "STATUS DE CONEXÃO: DISPOSITIVO NÃO EXISTE."
            Case 2
                Label5.Text = "STATUS DE CONEXÃO: DISPOSITIVO EM PROCESSO."
            Case 7
                Label5.Text = "STATUS DE CONEXÃO: ERRO NA ESCRITA DA PORTA"
            Case 8
                Label5.Text = "STATUS DE CONEXÃO: ERRO NA LEITURA DA PORTA."
            Case 11
                Label5.Text = "STATUS DE CONEXÃO: ERRO DESCONHECIDO NA PORTA SERIAL."
            Case -21
                Label5.Text = "STATUS DE CONEXÃO: DISPOSITIVO NÃO ENCONTRADO."
            Case -22
                Label5.Text = "STATUS DE CONEXÃO: ERRO NA ABERTURA DA PORTA USB."
            Case -23
                Label5.Text = "STATUS DE CONEXÃO: ERRO DE CLAIM UL"
            Case -24
                Label5.Text = "STATUS DE CONEXÃO: ERRO NA ESCRITA DA PORTA"
            Case -31
                Label5.Text = "STATUS DE CONEXÃO: ERRO NA CONEXÃO TCP"
            Case Else
                Label5.Text = "STATUS DE CONEXÃO: STATUS NÃO REGISTRAR. FAVOR VERIFICAR."
        End Select
    End Sub
End Class
