﻿Imports System.Runtime.InteropServices

Public Class FunctionsDll

    <DllImport("E1_Impressora.dll", EntryPoint:="AbreConexaoImpressora", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function AbreConexaoImpressora(ByVal tipo As Integer, ByVal modelo As String, ByVal conexao As String, ByVal parametro As Integer) As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="FechaConexaoImpressora", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function FechaConexaoImpressora() As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="ImpressaoTexto", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function ImpressaoTexto(<MarshalAs(UnmanagedType.LPStr)> dados As String, ByVal posicao As Integer, ByVal stilo As Integer, ByVal tamanho As Integer) As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="Corte", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function Corte(ByVal avanco As Integer) As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="ImpressaoQRCode", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function ImpressaoQRCode(<MarshalAs(UnmanagedType.LPStr)> dados As String, ByVal tamanho As Integer, ByVal nivelCorrecao As Integer) As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="ImpressaoCodigoBarras", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function ImpressaoCodigoBarras(ByVal tipo As Integer, ByVal dados As String, ByVal altura As Integer, ByVal largura As Integer, ByVal HRI As Integer) As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="AvancaPapel", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function AvancaPapel(ByVal linhas As Integer) As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="LimpaBuffer", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function LimpaBuffer() As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="StatusImpressora", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function StatusImpressora(ByVal param As Integer) As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="AbreGaveta", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function AbreGaveta() As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="InicializaImpressora", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function InicializaImpressora() As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="DefinePosicao", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function DefinePosicao(ByVal posicao As Integer) As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="SinalSonoro", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function SinalSonoro(ByVal qtd As Integer, ByVal tempoInício As Integer, ByVal tempoFim As Integer) As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="TesteImpressora", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function TesteImpressora() As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="ImprimeXMLSAT", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function ImprimeXMLSAT(<MarshalAs(UnmanagedType.LPStr)> dados As String) As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="ImprimeXMLCancelamentoSAT", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function ImprimeXMLCancelamentoSAT(<MarshalAs(UnmanagedType.LPStr)> dados As String, <MarshalAs(UnmanagedType.LPStr)> assQRCode As String) As Integer
    End Function

    <DllImport("E1_Impressora.dll", EntryPoint:="ImprimeXMLNFCe", CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi)>
    Public Shared Function ImprimeXMLNFCe(<MarshalAs(UnmanagedType.LPStr)> dados As String) As Integer
    End Function

End Class
